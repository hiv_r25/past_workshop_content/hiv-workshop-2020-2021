

# version 2019/07/15;  email tina.davenport@duke.edu with questions or issues


##################################################################################################
##
##  This function will create a standard demographic table with summaries stratifed by a
##  given arm (for example treatment, or sex, or disease status).
##  See below for an example. The <NA>s will disappear when coverting to latex or html.
##
##                                   Overall     Arm_setosa Arm_versicolor   Arm_virginica    P
##  n                                    150             50             50              50 <NA>
##  Sepal.Width N=                       150             50             50              50    0
##  Sepal.Width Mean (SD)        3.06 (0.44)    3.43 (0.38)    2.77 (0.31)     2.97 (0.32) <NA>
##  Sepal.Width Median [IQR]     3 [2.8-3.3] 3.4 [3.2-3.68]   2.8 [2.52-3]    3 [2.8-3.18] <NA>
##  Sepal.Width (Min, Max)          (2, 4.4)     (2.3, 4.4)       (2, 3.4)      (2.2, 3.8) <NA>
##  Sepal.Length N=                      150             50             50              50    0
##  Sepal.Length Mean (SD)       5.84 (0.83)    5.01 (0.35)    5.94 (0.52)     6.59 (0.64) <NA>
##  Sepal.Length Median [IQR]  5.8 [5.1-6.4]    5 [4.8-5.2]  5.9 [5.6-6.3]  6.5 [6.23-6.9] <NA>
##  Sepal.Length (Min, Max)       (4.3, 7.9)     (4.3, 5.8)       (4.9, 7)      (4.9, 7.9) <NA>
##  Petal.Width N=                       150             50             50              50    0
##  Petal.Width Mean (SD)         1.2 (0.76)    0.25 (0.11)     1.33 (0.2)     2.03 (0.27) <NA>
##  Petal.Width Median [IQR]   1.3 [0.3-1.8]  0.2 [0.2-0.3]  1.3 [1.2-1.5]     2 [1.8-2.3] <NA>
##  Petal.Width (Min, Max)        (0.1, 2.5)     (0.1, 0.6)       (1, 1.8)      (1.4, 2.5) <NA>
##  Petal.Length N=                      150             50             50              50    0
##  Petal.Length Mean (SD)       3.76 (1.77)    1.46 (0.17)    4.26 (0.47)     5.55 (0.55) <NA>
##  Petal.Length Median [IQR] 4.35 [1.6-5.1] 1.5 [1.4-1.58]   4.35 [4-4.6] 5.55 [5.1-5.88] <NA>
##  Petal.Length (Min, Max)         (1, 6.9)       (1, 1.9)       (3, 5.1)      (4.5, 6.9) <NA>
##  color (n=150)                       <NA>           <NA>           <NA>            <NA> 0.78
##  colorPink                        48 (32)        18 (36)        15 (30)         15 (30) <NA>
##  colorPurple                   53 (35.33)        19 (38)        18 (36)         16 (32) <NA>
##  colorWhite                    49 (32.67)        13 (26)        17 (34)         19 (38) <NA>
## 
##################################################################################################




############################################################################
##                                                                        ##
##     Function to create Demographic Tables                              ##
##                                                                        ##
##  :::INPUT:::                                                           ##
##                                                                        ##
##  DtaFrm: A data frame containing all the demographic variables.        ##
##  ArmInd: Either a character string or a vector specifying the          ##
##      stratification/intervention arm                                   ##
##  ord: A data.frame with two columns named "var" and "flag". "var" is   ##
##      a character vector of variable names. "flag" indicates if it is   ##
##      a categorical variable to compute frequency and percent (flag=1)  ##
##      or a continuous variable to compute mean, sd, median, IQR, min,   ##
##      and max (flag=0).                                                 ##
##  NP: Logical. Should non-parametric tests be used (KW and Fisher) or   ##
##      parametric tests (ANOVA and chi-square)?                          ##
##  na: Logical. Should missing values be included in the crosstabs of    ##
##      categorical variables (but not the p-values)?                     ##
##  rpct: Logical. Should the percentages for categorical variables be    ##
##      row percentages? (default is FALSE)                               ##
##                                                                        ##
##                                                                        ##
##  :::OUTPUT:::                                                          ##
##                                                                        ##
##  A data frame of 1) Mean (SD), Median [IQR], and (Min, Max) of the     ##
##  continuous variables for each intervention arm and the p-value for    ##
##  the ANOVA or Kruskal�Wallis test, and 2) the counts and percentages   ##
##  of the levels of categorical variables for each arm, and a p-value    ##
##  for the chi-square or Fisher's exact test.                            ##
##                                                                        ##
##                                                                        ##
##  :::NOTES:::                                                           ##
##                                                                        ##
##  Updated!  For na=F, a missing row is presented in the table, but the  ##
##            percentages do not include missing values (just give NA).   ##
##            Added the option to compute row percentages instead of      ##
##            column percentages (default is FALSE for row percent)       ##
##  Updated!  Compute Mean (SD), Median [IQR], and (Min, Max) for         ##
##            continuous variables (no more MVs and MdVs)                 ##
##            Changed the way the variables are inputed so that the       ##
##            order of the variables is retained.                         ##
##                                                                        ##
############################################################################
MkTblNew <- function(DtaFrm, ArmInd, ord, NP=F, na=T, dig=2, rpct=FALSE, ...){

    if(any(names(ord) != c("var", "flag"))){
        stop("ord names must be 'var' and 'flag', respectively")}

    if(length(ArmInd)==1){
        ArmInd <- factor(DtaFrm[,match(ArmInd, colnames(DtaFrm) )])
    }

    MeanSD <- function(x, dgts=2, ...){
        mm  <- round(mean(x, ...), dgts)
        std <- round(sd(x, ...), dgts)
        rslt <- paste(mm, " (", std, ")", sep="")   # mean (SD)
        return(rslt)
    }
    MedRange <- function(x, dgts=2, ...){
        mm  <- round(median(x, ...),dgts)
        rng1 <- round(quantile(x, ...)[2],dgts)
        rng2 <- round(quantile(x, ...)[4],dgts)
        rslt <- paste(mm, " [", rng1,"-", rng2, "]", sep="")
        return(rslt)
    }
    MinMax <- function(x, dgts=2, ...){
        m1 <- round(min(x, ...), dgts)
        m2 <- round(max(x, ...), dgts)
        rslt <- paste0("(", m1, ", ", m2, ")")
        return(rslt)
    }
    NN <- function(x){sum(!is.na(x))}

    N <- sum(table(ArmInd))           # total randomized
    noArms <- length(unique(ArmInd))  # number of trt arms
    
    rslt <- matrix(c(N,table(ArmInd), NA), nrow=1) 
      rownames(rslt) <- "n"           # initialze results table

    Cols <- match(ord$var, colnames(DtaFrm))
    for(ee in 1:length(Cols)){

        if(ord$flag[ee]==0){
            Expose <- DtaFrm[,Cols[ee]]
            Ns <- c(NN(Expose), tapply(X=Expose, INDEX=ArmInd, FUN=NN))
            Mns <- c(MeanSD(Expose, dgts=dig, na.rm=T),
                tapply(X=Expose, INDEX=ArmInd, FUN=MeanSD, 
                na.rm=T, dgts=dig))
            Mds <- c(MedRange(Expose, dgts=dig, na.rm=T),
                tapply(X=Expose, INDEX=ArmInd, FUN=MedRange, 
                na.rm=T, dgts=dig))
            MiMa <- c(MinMax(Expose, dgts=dig, na.rm=T),
                tapply(X=Expose, INDEX=ArmInd, FUN=MinMax, 
                na.rm=T, dgts=dig))
            if(!isTRUE(NP)){ out <- oneway.test(Expose ~ ArmInd)
            } else {out <- kruskal.test(Expose ~ ArmInd)}
            tmp <- cbind(matrix(c(Ns, Mns, Mds, MiMa), nrow=4, byrow=T),
                c(round(out$p.value,dig), NA, NA, NA))

            rownames(tmp) <- paste0(names(DtaFrm)[Cols[ee]],
                c(" N=", " Mean (SD)", " Median [IQR]", " (Min, Max)"))
            rslt <- rbind(rslt,tmp)
 
        } else if(ord$flag[ee]==1){
            Expose1 <- factor(DtaFrm[,Cols[ee]])  # no NAs in factor
            Expose <- addNA(Expose1, ifany=T)     # NAs as level

            # hypothesis tests with NAs excluded
            if(!isTRUE(NP)){ X2 <- chisq.test(table(Expose1, ArmInd))
            } else {X2 <- fisher.test(table(Expose1, ArmInd))}  # Exact Test
            pvals <- as.character(round(X2$p.value,dig))

            CTS <- table(Expose, ArmInd)  # counts of exposure by factor
            totct <- table(Expose)        # counts overall
            nofac <- length(unique(levels(Expose)))  # number factors

            if(!isTRUE(na)){  # don't include NAs in percentages
                tmp <- table(Expose1, ArmInd)
                Nf <- sum(!is.na(Expose1))
                totpct <- round(table(Expose1) / sum(table(Expose1))*100, dig)
                if(isTRUE(rpct)){  # row percentages
                    pcts <- round( tmp/c(rowSums(tmp))
                        *100,dig)  # percentages not including NA
                    # Only add row of NA percent if there are NAs!
                    if(any(is.na(Expose1))){
                        NAtot <- tapply(is.na(Expose1), ArmInd, sum)
                        pcts <- rbind(pcts, round(NAtot/sum(NAtot)*100, 2))
                        totpct <- c(totpct, NA)
                    }          
                } else if(!isTRUE(rpct)){
                    pcts <- round( t(t(tmp)/c(colSums(tmp)))
                        *100,dig)  # percentages not including NA
                    totpct <- round(table(Expose1) / sum(table(Expose1))*100,
                        dig)
                    # Only add row of NA percent if there are NAs!
                    if(any(is.na(Expose1))){
                        pcts <- rbind(pcts, NA)
                        totpct <- c(totpct, NA)
                    }
                }
            } else{
                tmp <- table(Expose, ArmInd)
                Nf <- sum(!is.na(Expose))
                if(isTRUE(rpct)){  # row percentages
                    pcts <- round( tmp/rowSums(tmp) *100,dig)
                } else if(!isTRUE(rpct)){
                    pcts <- round( t(t(tmp)/c(table(ArmInd))) *100,dig)
                }
                totpct <- round(totct / N*100, dig)
            }

            dfm <- matrix(c(paste0(CTS, " (", pcts, ")"), 
                rep(NA, nofac)), ncol=noArms+1)
            dfm <- cbind(matrix(paste0(totct," (",totpct,")"),ncol=1),dfm)
            dfm <- rbind(c(rep(NA, dim(dfm)[2]-1),pvals), dfm)
            rownames(dfm) <- c(paste0(names(DtaFrm)[Cols[ee]], " (n=", Nf, 
                ")"),paste0(names(DtaFrm)[Cols[ee]],unique(levels(Expose))))
            rslt <- rbind(rslt,dfm)
        }
    }
    fin <- data.frame(rslt)
      colnames(fin) <- c("Overall", paste0("Arm_",names(table(ArmInd))), "P")
    return(fin)
}
############################################################################






