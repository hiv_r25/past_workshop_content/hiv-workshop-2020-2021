## scRNA-Seq
1. [Background Slides](slides/scrna_background.pdf)
1. [Bioinformatic Analysis: PBMC 1k](notebooks/cellranger_count_pbmc1k.ipynb)
1. [Statistical Analysis Slides](slides/scRNAseq_analysis.pdf)
1. [Statistical Analysis: Seurat](notebooks/scRNA-seq_tutorial-seurat-version.ipynb)
1. [Statistical Analysis: Scanpy](notebooks/scRNA-seq_tutorial-scanpy-version.ipynb)

## Appendix
1. [Configuration File](notebooks/config.sh)
2. [Download Data](notebooks/setup/download_10x_data.ipynb)
