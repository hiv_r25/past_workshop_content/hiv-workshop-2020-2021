 <!-- a normal html comment 
- [CSP2 Schedule](content/csp2_schedule.md)
-->

# Reproducible Analysis
- [Reproducible Analysis Lecture](reproducible/reproducible_research_lecture.md)
- [Git Introduction](reproducible/git_overview.md)
    
# R Introduction

- [R Basics](datascience/)
